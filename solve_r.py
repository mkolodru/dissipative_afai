# solve level stats of model from titum et al.

import numpy
import sys
import numpy.linalg as la
import scipy.sparse.linalg
import pylab
import scipy.sparse as sp
import numpy.random as rr
argc=3
if len(sys.argv) < argc:
    print('Usage: solve_r.py $W $rand_seed')
    sys.exit()

L_x=40 # Use convention L_x and L_y are number of sites, not unit cell
L_y=40
Omega=1.0
T=2*numpy.pi/Omega
J=1.25*Omega # Fine-tuned value
lam=numpy.pi
W=float(sys.argv[1])
rand_seed=int(sys.argv[2])
rr.seed(rand_seed)

def hash(x,y,L_x):
    return x+y*L_x

print('Constructing matrices...')

H_1=sp.lil_matrix((L_x*L_y,L_x*L_y))
H_2=sp.lil_matrix((L_x*L_y,L_x*L_y))
H_3=sp.lil_matrix((L_x*L_y,L_x*L_y))
H_4=sp.lil_matrix((L_x*L_y,L_x*L_y))
H_5=sp.lil_matrix((L_x*L_y,L_x*L_y)) # For now this will just be the static bit

D_coeff=lam/(2*T)

for x in range(0,L_x):
    for y in range(0,L_y):
        if (x+y) % 2 == 0:
            H_1[hash(x,y,L_x),hash(x,(y+1)%L_y,L_x)]=-J
            H_1[hash(x,(y+1)%L_y,L_x),hash(x,y,L_x)]=-J
            H_4[hash(x,y,L_x),hash((x+1)%L_x,y,L_x)]=-J
            H_4[hash((x+1)%L_x,y,L_x),hash(x,y,L_x)]=-J
        else:
            H_3[hash(x,y,L_x),hash(x,(y+1)%L_y,L_x)]=-J
            H_3[hash(x,(y+1)%L_y,L_x),hash(x,y,L_x)]=-J
            H_2[hash(x,y,L_x),hash((x+1)%L_x,y,L_x)]=-J
            H_2[hash((x+1)%L_x,y,L_x),hash(x,y,L_x)]=-J
        H_5[hash(x,y,L_x),hash(x,y,L_x)]=W*(2*rr.random()-1) + D_coeff*(-1)**(x+y)

H_1=H_1+H_5
H_2=H_2+H_5
H_3=H_3+H_5
H_4=H_4+H_5

if L_x < 5 and L_y < 5:
    for j in range(L_x*L_y):
        for k in range(L_x*L_y):
            if abs(H_1[j,k])>0.01:
                print('H_1['+str(j)+','+str(k)+'] = '+str(H_1[j,k]))
    for j in range(L_x*L_y):
        for k in range(L_x*L_y):
            if abs(H_2[j,k])>0.01:
                print('H_2['+str(j)+','+str(k)+'] = '+str(H_2[j,k]))
    for j in range(L_x*L_y):
        for k in range(L_x*L_y):
            if abs(H_3[j,k])>0.01:
                print('H_3['+str(j)+','+str(k)+'] = '+str(H_3[j,k]))
    for j in range(L_x*L_y):
        for k in range(L_x*L_y):
            if abs(H_4[j,k])>0.01:
                print('H_4['+str(j)+','+str(k)+'] = '+str(H_4[j,k]))
    for j in range(L_x*L_y):
        for k in range(L_x*L_y):
            if abs(H_5[j,k])>0.01:
                print('H_5['+str(j)+','+str(k)+'] = '+str(H_5[j,k]))

print('Solving time evolution...')

U=numpy.identity(L_x*L_y,dtype=complex)
U=scipy.sparse.linalg.expm_multiply(-1.j*H_1*T/5.,U)
U=scipy.sparse.linalg.expm_multiply(-1.j*H_2*T/5.,U)
U=scipy.sparse.linalg.expm_multiply(-1.j*H_3*T/5.,U)
U=scipy.sparse.linalg.expm_multiply(-1.j*H_4*T/5.,U)
U=scipy.sparse.linalg.expm_multiply(-1.j*H_5*T/5.,U)

if L_x < 5 and L_y < 5:
    for j in range(L_x*L_y):
        for k in range(L_x*L_y):
            if abs(U[j,k])>0.01:
                print('U['+str(j)+','+str(k)+'] = '+str(U[j,k]))


print('Diagonalizing...')

(Ueig,V)=la.eig(U)
E_f=numpy.sort(numpy.real(numpy.log(Ueig)/(-1.j*T)))

gap=E_f[1:]-E_f[:-1]
r=numpy.minimum(gap[1:],gap[:-1])/numpy.maximum(gap[1:],gap[:-1])

numpy.save('r.npy',r)
